import {Component, ViewChild} from '@angular/core';
import {Content, NavController} from 'ionic-angular';
import {Store} from "@ngrx/store";
import {State} from "../../store";
import * as task from "../../store/actions/task.actions";
import {Task} from "../../models/Task";
import * as _ from "lodash";

@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
})
export class HomePage {
  private tasks: Task[];
  public task: Task;
  public bgColor: string;

  @ViewChild('content')content:Content;

  constructor(public navCtrl: NavController, private store: Store<State>) {
    this.store.dispatch(new task.FetchTaskFromRemote());
    this.store.select('tasks').subscribe(tasks => {
      this.tasks = tasks;
      this.nxtTsk();
    });
  }

  nxtTsk() {
    this.task = _.shuffle(this.tasks).pop();
    this.pastelColors();
  }

  pastelColors(){
    var r = (Math.round(Math.random()* 127) + 127).toString(16);
    var g = (Math.round(Math.random()* 127) + 127).toString(16);
    var b = (Math.round(Math.random()* 127) + 127).toString(16);
    this.bgColor = '#' + r + g + b;
  }

}
