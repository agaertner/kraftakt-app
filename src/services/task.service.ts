import { Injectable } from '@angular/core';
import { Platform } from 'ionic-angular';
import { Observable } from 'rxjs/rx';
import { Task } from '../models/task';
import PouchDB from 'pouchdb';

@Injectable()
export class TaskService {
  private db;

  constructor(private platform: Platform) { }

  initDB() : Promise<any> {
    return this.platform.ready()
      .then(() => {
        this.db = new PouchDB('task', { adapter: 'websql' });
      });
  }

  add(task: Task) : Promise<any> {
    return this.db.post(task);
  }

  checkTask(task: Task) : Promise<boolean> {
    return new Promise<boolean>((resolve, reject) => {
      this.getAll().map(docs => docs.find(doc => doc.content == task.content))
        .subscribe(result => {
          resolve(!!result);
        })
    })
  }

  update(task: Task) : Promise<any> {
    return this.db.put(task);
  }

  delete(task: Task) : Promise<any> {
    return this.db.remove(task);
  }

  getAll() : Observable<any> {
    return Observable.fromPromise(
      this.initDB()
        .then(() => {
          return this.db.allDocs({ include_docs: true });
        })
        .then(docs => {

          // Each row has a .doc object and we just want to send an
          // array of task objects back to the calling code,
          // so let's map the array to contain just the .doc objects.
          return docs.rows.map(row => {
            // Convert string to date, doesn't happen automatically.
            row.doc.Date = new Date(row.doc.Date);
            return row.doc;
          });
        }));
  }

  getChanges(): Observable<any> {
    return Observable.create(observer => {

      // Listen for changes on the database.
      this.db.changes({ live: true, since: 'now', include_docs: true })
        .on('change', change => {
          // Convert string to date, doesn't happen automatically.
          change.doc.Date = new Date(change.doc.Date);
          observer.next(change.doc);
        });
    });
  }
}
