import { Injectable } from '@angular/core';
import { Effect, Actions } from '@ngrx/effects';
import { Observable } from 'rxjs/rx';

import { TaskService } from '../../services/task.service';
import { Task } from '../../models/task';
import * as task from '../actions/task.actions';
import {Http} from "@angular/http";

@Injectable()
export class TaskEffects {

  private static "REMOTE_BASE_HOST": string = "https://sweet-nightmare.firebaseio.com/";

  constructor(
    private actions$: Actions,
    private db: TaskService,
    private http: Http
  ) { }


  @Effect({dispatch:false}) fetchTasks$ = this.actions$
    .ofType(task.FETCH_TASKS_FROM_REMOTE)
    .switchMap(() => this.http.get(TaskEffects.REMOTE_BASE_HOST + '/tasks.json'))
    .map(res => res.json())
    .map((tasks:Task[])=> {
      tasks.map(t => {
        this.db.checkTask(t).then(r => {
          if(!r){
            this.db.add(t);
          }
        }).catch(e => console.log(e))
      });
    });

  @Effect() addTask$ = this.actions$
    .ofType(task.ADD_TASK)
    .map((action: task.AddTask) => action.payload)
    .mergeMap(task => this.db.add(task));

  @Effect() updateTask$ = this.actions$
    .ofType(task.UPDATE_TASK)
    .map((action: task.UpdateTask) => action.payload)
    .mergeMap(task => this.db.update(task));

  @Effect() deleteTask$ = this.actions$
    .ofType(task.DELETE_TASK)
    .map((action: task.DeleteTask) => action.payload)
    .mergeMap(task => this.db.delete(task));


  allTasks$ = this.db.getAll()
    .map(tasks => new task.LoadTasksSuccess(tasks));


  changedTasks$ = this.db.getChanges()
    .map(change => {
      if (change._deleted) {
        return new task.DeleteTaskSuccess(change._id);
      }
      else {
        return new task.AddUpdateTaskSuccess(change);
      }
    });

  @Effect() getTasks$ = Observable.concat(this.allTasks$, this.changedTasks$);
}
