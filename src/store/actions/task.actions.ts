import {Action} from '@ngrx/store';
import {Task} from "../../models/Task";

export const ADD_TASK = 'ADD_TASK';
export const UPDATE_TASK = 'UPDATE_TASK';
export const DELETE_TASK = 'DELETE_TASK';
export const FETCH_TASKS_FROM_REMOTE = 'FETCH_TASKS_FROM_REMOTE';
export const LOAD_TASKS_SUCCESS = 'LOAD_TASKS_SUCCESS';
export const ADD_UPDATE_TASK_SUCCESS = 'ADD_UPDATE_TASK_SUCCESS';
export const DELETE_TASK_SUCCESS = 'DELETE_TASK_SUCCESS';


export class AddTask implements Action {
  readonly type = ADD_TASK;

  constructor(public payload: Task) {
  }
}

export class UpdateTask implements Action {
  readonly type = UPDATE_TASK;

  constructor(public payload: Task) {
  }
}

export class DeleteTask implements Action {
  readonly type = DELETE_TASK;

  constructor(public payload: Task) {
  }
}

export class FetchTaskFromRemote implements Action {
  readonly type = FETCH_TASKS_FROM_REMOTE;

  constructor(public payload: any = null) {
  }
}

export class LoadTasksSuccess implements Action {
  readonly type = LOAD_TASKS_SUCCESS;

  constructor(public payload: Task[]) {
  }
}

export class AddUpdateTaskSuccess implements Action {
  readonly type = ADD_UPDATE_TASK_SUCCESS;

  constructor(public payload: Task) {
  }
}

export class DeleteTaskSuccess implements Action {
  readonly type = DELETE_TASK_SUCCESS;

  constructor(public payload: number) {
  }
}

export type Actions =
  | AddTask
  | UpdateTask
  | DeleteTask
  | FetchTaskFromRemote
  | LoadTasksSuccess
  | AddUpdateTaskSuccess
  | DeleteTaskSuccess;
