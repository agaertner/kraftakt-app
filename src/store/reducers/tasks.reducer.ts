import * as task from '../actions/task.actions';
import { Task } from '../../models/task';
export interface State extends Array<Task>{}
export const initialState: State = [];

export function reducer (
  state:State = initialState,
  action: task.Actions
): Task[] {
  switch(action.type) {
    case task.LOAD_TASKS_SUCCESS:
      return action.payload;
    case task.ADD_UPDATE_TASK_SUCCESS:
      let exists = state.find(task => task._id === action.payload._id);

      if (exists) {
        // UPDATE
        return state.map(task => {
          return task._id === action.payload._id ? Object.assign({}, task, action.payload) : task;
        });
      }
      else {
        // ADD
        return [...state, Object.assign({}, action.payload)];
      }
    case task.DELETE_TASK_SUCCESS:
      return state.filter(task => task._id !== action.payload);
    default:
      return state;
  };
}
