import {Task} from "../models/Task";
import * as fromTask from "./reducers/tasks.reducer";

import {ActionReducerMap} from "@ngrx/store";

export interface State {
  tasks: Task[];
}

/**
 * Our state is composed of a map of action reducer functions.
 * These reducer functions are called with each dispatched action
 * and the current or initial state and return a new immutable state.
 */
export const reducers: ActionReducerMap<State> = {
  tasks: fromTask.reducer,
};
