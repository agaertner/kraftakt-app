/**
 * Created by Alex on 24.10.17.
 */
export class Task {
  _id: number;
  content: string;
  type: string;
  tags: Tag[];
}

interface Tag {
  name: string;
}
